Source: r-cran-genieclust
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-genieclust
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-genieclust.git
Homepage: https://cran.r-project.org/package=genieclust
Rules-Requires-Root: no

Package: r-cran-genieclust
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R Genie++ Hierarchical Clustering Algorithm with Noise Points Detection
 A retake on the Genie algorithm - a robust hierarchical clustering
 method (Gagolewski, Bartoszuk, Cena, 2016
 <DOI:10.1016/j.ins.2016.05.003>). Now faster and more memory efficient;
 determining the whole hierarchy for datasets of 10M points in low
 dimensional Euclidean spaces or 100K points in high-dimensional ones
 takes only 1-2 minutes. Allows clustering with respect to mutual
 reachability distances so that it can act as a noise point detector or a
 robustified version of 'HDBSCAN*' (that is able to detect a predefined
 number of clusters and hence it does not dependent on the somewhat
 fragile 'eps' parameter).
 .
 The package also features an implementation of economic inequity indices
 (the Gini, Bonferroni index) and external cluster validity measures
 (partition similarity scores; e.g., the adjusted Rand, Fowlkes-Mallows,
 adjusted mutual information, pair sets index).
 .
 See also the 'Python' version of 'genieclust' available on 'PyPI', which
 supports sparse data, more metrics, and even larger datasets.
